import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import java.text.*;
import java.applet.*;

class High extends Thread {
	Highway H;
	String showString;
	private JLabel showLabel;
	boolean death_flag;
	High(Highway www, JLabel lbl) {
		H = www;
		showLabel = lbl;
	}
	public void run() {
		showString = H.getStatus();
		showLabel.setText(showString);
		H.NewCarOrNot();
		H.NewInterCarOrNot();
		H.SetSpeedForAllCar(1);
		death_flag = H.MoveAllCar();
		if(death_flag == true) {
			showString = "Car Crash !!!";
			showLabel.setText(showString);
		}
	}
}

class POODemo2 extends JPanel implements ActionListener {

	private JLabel showLabel;
	private JLabel lab;
	private String showString = " ";
	private String show2 = "";
	boolean Set = false;
	boolean death_flag;
	Highway H;
	Highway H2;
	Highway H3;
	Highway H41;
	Highway H42;
	High[] tarr;
	private int count;
	private int taskNum = 3;
	private int highwayLen = 200;
	private int beginCarNum = 100;
	private int interCarNum = 100;

	// Labels to identify the fields.
	private JLabel taskNumLabel;
	private JLabel highwayLenLabel;
	private JLabel beginCarNumLabel;
	private JLabel interCarNumLabel;

	// Strings for the labels.
	private static String taskString = "Task Number (1~4): ";
	private static String highwayLenString = "Highway Length: ";
	private static String beginCarString = "Car Number (at begin): ";
	private static String interCarString = "Car Number (at intersection): ";

	// Fields for data entry.
	private JFormattedTextField taskNumField;
	private JFormattedTextField highwayLenField;
	private JFormattedTextField beginCarNumField;
	private JFormattedTextField interCarNumField;

	public POODemo2() {
		super(new BorderLayout());

		tarr = new High[2];

		// Create the labels.
		taskNumLabel = new JLabel(taskString);
		highwayLenLabel = new JLabel(highwayLenString);
		beginCarNumLabel = new JLabel(beginCarString);
		interCarNumLabel = new JLabel(interCarString);

		// Create the text fields.
		taskNumField = new JFormattedTextField(createFormatter("#"));
		//		taskNumField.setValue(new Integer(taskNum));
		taskNumField.setColumns(10);
		taskNumField.addActionListener(this);

		highwayLenField = new JFormattedTextField(createFormatter("###"));
		//		highwayLenField.setValue(new Integer(highwayLen));
		highwayLenField.setColumns(10);
		highwayLenField.addActionListener(this);

		beginCarNumField = new JFormattedTextField(createFormatter("###"));
		//		beginCarNumField.setValue(new Integer(beginCarNum));
		beginCarNumField.setColumns(10);
		beginCarNumField.addActionListener(this);

		interCarNumField = new JFormattedTextField(createFormatter("###"));
		//		interCarNumField.setValue(new Integer(interCarNum));
		interCarNumField.setColumns(10);
		interCarNumField.addActionListener(this);

		// Tell accessibility tools about label/textfield pairs.
		taskNumLabel.setLabelFor(taskNumField);
		highwayLenLabel.setLabelFor(highwayLenField);
		beginCarNumLabel.setLabelFor(beginCarNumField);
		interCarNumLabel.setLabelFor(interCarNumField);

		JPanel showPane = new JPanel(new GridLayout(0, 1));
		showLabel = new JLabel(showString);
		lab = new JLabel(show2);
		showPane.add(showLabel);
		showPane.add(lab);


		// Lay out the labels in a panel.
		JPanel labelPane = new JPanel(new GridLayout(0, 1));
		labelPane.add(taskNumLabel);
		labelPane.add(highwayLenLabel);
		labelPane.add(beginCarNumLabel);
		labelPane.add(interCarNumLabel);

		// Layout the text fields in a panel.
		JPanel fieldPane = new JPanel(new GridLayout(0, 1));
		fieldPane.add(taskNumField);
		fieldPane.add(highwayLenField);
		fieldPane.add(beginCarNumField);
		fieldPane.add(interCarNumField);

		// Put the panels in this panel, labels on left,
		// text fields on right.
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
		add(labelPane, BorderLayout.CENTER);
		add(fieldPane, BorderLayout.LINE_END);

		add(showPane, BorderLayout.NORTH);

		add(createButtons(), BorderLayout.SOUTH);
	}
	protected MaskFormatter createFormatter(String s) {
		MaskFormatter formatter = null;
		try {
			formatter = new MaskFormatter(s);
		} catch (java.text.ParseException exc) {
			System.err.println("formatter is bad: " + exc.getMessage());
			System.exit(-1);
		}
		return formatter;
	}

	public void actionPerformed(ActionEvent e) {
		if(Set == false) {			
			if("clear".equals(e.getActionCommand())) {
				Set = false;
				taskNumField.setValue(null);
				highwayLenField.setValue(null);
				beginCarNumField.setValue(null);
				interCarNumField.setValue(null);
				showString = " ";
				showLabel.setText(showString);
				lab.setText(showString);
				count = 0;
			}
			else {
				Set = true;
				String s = (String) taskNumField.getValue();
				try {
					taskNum = Integer.parseInt(s);
				} catch(NumberFormatException task_e) { }

				s = (String) highwayLenField.getValue();
				try {
					highwayLen = Integer.parseInt(s);
				} catch(NumberFormatException high_e) {	}

				s = (String) beginCarNumField.getValue();
				try {
					beginCarNum = Integer.parseInt(s);
				} catch(NumberFormatException begin_e) { }

				s = (String) interCarNumField.getValue();
				try {
					interCarNum = Integer.parseInt(s);
				} catch(NumberFormatException inter_e) { }

				switch(taskNum) {
					case 1:
						H = new Highway(highwayLen, beginCarNum, interCarNum);
						break;
					case 2:
						H2 = new Highway(highwayLen, beginCarNum, interCarNum);
						break;
					case 3:
						H3 = new Highway(highwayLen, beginCarNum, interCarNum);
						break;
					case 4:
						H41 = new Highway(highwayLen, beginCarNum, interCarNum);
						H42 = new Highway(highwayLen + 10, beginCarNum, interCarNum);
						break;
					default:
						break;
				}

			}
		}
		else {
			if("clear".equals(e.getActionCommand())) {
				Set = false;
				taskNumField.setValue(null);
				highwayLenField.setValue(null);
				beginCarNumField.setValue(null);
				interCarNumField.setValue(null);
				showString = " ";
				showLabel.setText(showString);
				lab.setText(showString);
				count = 0;
			}
			else {
				Set = true;
				switch(taskNum) {
					case 1:
						showString = H.getStatus();
						showLabel.setText(showString);
						H.NewCarOrNot();
						H.SetSpeedForAllCar(1);
						death_flag = H.MoveAllCar();
						if(death_flag == true) {
							Set = false;
							showString = "Car Crash !!!";
							showLabel.setText(showString);
							break;
						}
						break;
					case 2:
						if(count == 4) {
							showString = H2.getStatus();
							showLabel.setText(showString);
							H2.NewCarOrNot();
							H2.SetSpeedForAllCar(0);
							death_flag = H2.MoveAllCar();
						}
						else {
							showString = H2.getStatus();
							showLabel.setText(showString);
							H2.NewCarOrNot();
							H2.SetSpeedForAllCar(1);
							death_flag = H2.MoveAllCar();
						}
						if(death_flag == true) {
							Set = false;
							showString = "Car Crash !!!";
							showLabel.setText(showString);
							break;
						}
						count++;
						break;
					case 3:
						showString = H3.getStatus();
						showLabel.setText(showString);
						H3.NewCarOrNot();
						H3.NewInterCarOrNot();
						H3.SetSpeedForAllCar(1);
						death_flag = H3.MoveAllCar();
						if(death_flag == true) {
							Set = false;
							showString = "Car Crash !!!";
							showLabel.setText(showString);
							break;
						}
						break;
					case 4:
						tarr[0] = new High(H41, showLabel);
						tarr[0].start();
						tarr[1] = new High(H42, lab);
						tarr[1].start();
						break;
					default:
						break;
				}
			}
		}
	}

	protected JComponent createButtons() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
		JButton button = new JButton("Start (Next Time Step)");
		button.addActionListener(this);
		panel.add(button);

		button = new JButton("Reset");
		button.addActionListener(this);
		button.setActionCommand("clear");
		panel.add(button);

		return panel;
	}
}

public class POODemo extends Applet {
	private static void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("OOPHW5Demo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Add contents to the window.
		frame.add(new POODemo2());

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}
	public void init() {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
	public void stop() {
	}
	public void paint(Graphics g) {
	}
}
