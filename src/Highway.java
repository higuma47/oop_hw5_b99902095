/** This is the Highway object of OOP HW1.
 * It can simulate the highway status.
 * 
 * @author NTU CSIE b99902095 Lu
 * @version 3.0
 *
 */

public class Highway {
	private int highway_len;
	private int beginCarNum;
	private int interCarNum;
	private int highwayCarNum;
	private java.util.LinkedList<Car> Cars;
	private Car tmp;
	private char highway_status[];
	/** Constructor. */
	public Highway(int len, int begin, int inter) {
		highway_len = len;
		beginCarNum = begin;
		interCarNum = inter;
		highwayCarNum = 0;
		Cars = new java.util.LinkedList<Car>();
		highway_status = new char[len];
		int iii;
		for (iii = 0; iii < len; iii++)
			highway_status[iii] = '.';
	}
	/** Print out the highway status. */
	public void print() {
		System.out.println(String.valueOf(highway_status));
	}
	public String getStatus() {
		String s = String.valueOf(highway_status);
		return s;
	}
	/** The method is used to determine whether add a new car from intersection on the highway or not. */
	public void NewInterCarOrNot() {
		int ind;
		int di;
		int spe;
		Car n;
		Car t;
		Car back;
		if(interCarNum > 0) {
			if(highwayCarNum == 0 || highwayCarNum == 1) {
				n = new Car();
				n.SetPosition(50);
				n.AdjustSpeed(4);
				n.InitFutureSpeed();
				highwayCarNum++;
				interCarNum--;
				Cars.add(0, n);
			}
			else if(highway_status[49] == '.') {
				for(ind = 0; ind < highwayCarNum - 1; ind++) {
					t = Cars.get(ind);
					back = Cars.get(ind + 1);
					if(t.position > 50 && back.position < 50) {
						di = t.position - 50;
						if(di > 1) {
							spe = di / 2;
							if(spe > 4)
								spe = 4;
							n = new Car();
							n.SetPosition(50);
							n.AdjustSpeed(spe);
							n.InitFutureSpeed();
							highwayCarNum++;
							interCarNum--;
							Cars.add(ind + 1, n);
						}
					}
				}
			}
		}
	}
	/** The method is used to determine whether add a new car from begining on the highway or not. */
	public void NewCarOrNot() {
		Car newCar;
		tmp = Cars.peekLast();
		if(beginCarNum > 0 && (highwayCarNum == 0 || tmp.position > 2)) {
			newCar = new Car();
			newCar.SetPosition(1);

			if(highwayCarNum == 0) {
				newCar.AdjustSpeed(4);
			}
			else {
				int distance = tmp.position - 1;
				int speed = distance / 2;
				if(speed > 4)
					speed = 4;
				newCar.AdjustSpeed(speed);
			}
			newCar.InitFutureSpeed();
			highwayCarNum++;
			beginCarNum--;
			Cars.addLast(newCar);
		}
	}
	/** The method is used to adjust all cars' speed and determine whether each car will speed up or slow down. */
	public void SetSpeedForAllCar(int f) {
		int ii;
		int dis;
		int sp;
		Car now;
		Car fro;
		for(ii = 0; ii < highwayCarNum; ii++) {
			now = Cars.get(ii);
			if(ii == 0) {
				if(f == 1)
					now.AdjustSpeed(4);
				else if(f == 0)
					now.AdjustSpeed(2);
			}
			else {
				fro = Cars.get(ii - 1);
				if(now.flag == 0) {
					now.AdjustSpeed(now.future_speed);
					dis = fro.position - now.position;
					sp = dis / 2;
					if(sp > 4)
						sp = 4;
					if(now.speed < sp) {
						now.AdjustFlag(1);
						now.AdjustFutureSpeed(sp);
					}
					else if(now.speed > sp){
						now.AdjustFutureSpeed(sp);
					}
				}
				else if(now.flag > 0) {
					now.AdjustFlag(now.flag - 1);
				}
			}
			Cars.set(ii, now);
		}
	}
	/** Move the positions of all cars.*/
	public boolean MoveAllCar() {
		int index;
		Car pr;
		Car d;
		for (index = 0; index < highway_len; index++)
			highway_status[index] = '.';
		for(index = 0; index < highwayCarNum; index++) {
			pr = Cars.get(index);
			pr.MoveCar();
			if(index > 0 && pr.position <= highway_len) {
				d = Cars.get(index - 1);
				if(pr.position >= d.position)
					return true;
			}
			if(pr.position <= highway_len) {
				highway_status[pr.position - 1] = 'x';
			}
			else {
				pr.SetPosition(10000);
			}
			Cars.set(index, pr);
		}
		return false;
	}
}
