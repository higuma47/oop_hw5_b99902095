/** This is the car object of OOP HW1.
 * It can simulate the activity of each car on the highway.
 * 
 * @author NTU CSIE b99902095 Lu
 * @version 3.0
 *
 */

public class Car {
	/** Car current speed. */
	public int speed;
	/** Car future speed : The speed will be changed in t+1(slow down) or t+2(speed up) time step */
	public int future_speed;
	/** The flag is used to determine the car to speed up or slow down */
	public int flag;
	/** Car current position. */
	public int position;
	/** Used in task 4 (bonus). If it counts down to 0, the car is removed from the highway. */
	public int remain_time;
	/** The method is used to initialize future_speed of the car. */
	public void InitFutureSpeed() {
		future_speed = speed;
	}
	/** The method is used to set a new position of the car. */
	public void SetPosition(int pos) {
		position = pos;
	}
	/** The method is used to adjust the car speed. */
	public void AdjustSpeed(int s) {
		speed = s;
	}
	/** The method is used to adjust future_speed of the car. */
	public void AdjustFutureSpeed(int sp) {
		future_speed = sp;
	}
	/** The method is used to adjust the car flag. */
	public void AdjustFlag(int f) {
		flag = f;
	}
	/** The method is used to move the car to the new position. */
	public void MoveCar() {
		position = position + speed;
	}
	/** Used in task 4 (bonus). The method is used to count down the remaining time. */
	public void CountDown() {
		remain_time = remain_time - 1;
	}
}
