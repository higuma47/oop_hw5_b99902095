Compile and Run Steps

1. 將 src 中的 *.java 檔案和 manifest.mf 全放置到和 Makefile 同個目錄之下

2. make all

3. 以下會要助教輸入一些資料, 隨意填寫即可, 密碼部分也隨意填, 但必須記住填了什麼密碼, 之後問到密碼的部分都必須填一樣的密碼, 此步驟為產生 jar 檔案之後, 為其加上的 signer

4. 填完之後即可直接打開 index.html 執行